# This script will report the battery status of the standard Linux Battery Driver
#    (for an ReMarkable 2) to an MQTT server for use in Home Assistant.
# It is intended to be run as a service, to report the battery status every day at 00:00

from dotenv import load_dotenv
import os
import paho.mqtt.client as mqtt
import time
import sys
from socket import timeout

# Load MQTT Configuration from .env file
## ENV SCHEME: MQTT_SERVER, MQTT_PORT, MQTT_USER, MQTT_PASSWORD

load_dotenv()
MQTT_SERVER = os.getenv("MQTT_SERVER")
MQTT_PORT = os.getenv("MQTT_PORT")
MQTT_USER = os.getenv("MQTT_USER")
MQTT_PASSWORD = os.getenv("MQTT_PASSWORD")

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)

# replaces _ with blanks to create a natural name
def create_natural_name(name):
    return name.replace("_", " ").title()

# Home Assistant Configuration
HA_Discovery_Prefix = "homeassistant"
HA_Device_Name = "reMarkable_2"

# Get the Battery Status
Battery_Percent = ""
Battery_Chargcount = ""
Battery_ChargerMode = "Unknown"

with open("/sys/class/power_supply/max77818_battery/capacity", "r") as f:
    Battery_Percent = f.read().strip()

with open("/sys/class/power_supply/max77818_battery/cycle_count", "r") as f:
    Battery_Chargcount = f.read().strip()

with open("/sys/class/power_supply/max77818_battery/status", "r") as f:
    Battery_ChargerMode = f.read().strip()

# Connect to MQTT Server
client = mqtt.Client()
client.username_pw_set(MQTT_USER, MQTT_PASSWORD)

client.on_connect = on_connect
try:
    client.connect(MQTT_SERVER, int(MQTT_PORT), 60)
except timeout:
    print("timeout! exiting cleanly")
    exit(0)
except OSError as err:
    if err.errno == os.errno.EHOSTUNREACH:
        print("no route to host! exiting cleanly")
        exit(0)
except BaseException as err:
    print(f"Unexpected {err=}, {type(err)=}")
    raise

client.loop_start()
time.sleep(4)

# Setup Home Assistant Discovery
print("Setting up Home Assistant Discovery")
print("publishing to: " + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/xx/config")
client.publish(HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery_percent/config", '{"name": "' + create_natural_name(HA_Device_Name) + ' Battery Percent", "state_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_percent/state", "unit_of_measurement": "%", "value_template": "{{ value_json.value }}", "device_class": "battery", "json_attributes_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_percent/state", "unique_id": "' + HA_Device_Name + '_battery_percent", "device": {"name": "' + HA_Device_Name + '", "model": "reMarkable 2", "manufacturer": "reMarkable"}}', retain=True)

client.publish(HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery_chargcount/config", '{"name": "' + create_natural_name(HA_Device_Name) + ' Battery Charge Count", "state_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_chargcount/state", "unit_of_measurement": "times", "value_template": "{{ value_json.value }}", "device_class": "battery", "json_attributes_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_chargcount/state", "unique_id": "' + HA_Device_Name + '_battery_chargcount", "device": {"name": "' + HA_Device_Name + '", "model": "reMarkable 2", "manufacturer": "reMarkable"}}', retain=True)

client.publish(HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery_chargemode/config", '{"name": "' + create_natural_name(HA_Device_Name) + ' Battery Charge Mode", "state_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_chargemode/state", "value_template": "{{ value_json.value }}", "device_class": "battery", "json_attributes_topic": "homeassistant/sensor/' + HA_Device_Name + '/battery_chargemode/state", "unique_id": "' + HA_Device_Name + '_battery_chargemode", "device": {"name": "' + HA_Device_Name + '", "model": "reMarkable 2", "manufacturer": "reMarkable"}}', retain=True)

# Publish Battery Status
print("Publishing Battery Status")
client.publish("homeassistant/sensor/" + HA_Device_Name + "/battery_percent/state", '{"value": ' + Battery_Percent + '}')
client.publish("homeassistant/sensor/" + HA_Device_Name + "/battery_chargcount/state", '{"value": ' + Battery_Chargcount + '}')
client.publish("homeassistant/sensor/" + HA_Device_Name + "/battery_chargemode/state", '{"value": "' + Battery_ChargerMode + '"}')

# Disconnect from MQTT Server
client.loop_stop()
client.disconnect()

# Exit
print("Done")
exit()