//
// DEPRECATED
//

// This script will report the battery status of the standard Linux Battery Driver 
//    (for an ReMarkable 2) to an MQTT server for use in Home Assistant.
// It is intended to be run as a service, to report the battery status every day at 00:00

#include <iostream>
#include <fstream>

// MQTT Config
std::string MQTT_SERVER = "";
std::string MQTT_PORT = "";
std::string MQTT_USER = "";
std::string MQTT_PASS = "";

// HomeAssistant Config
std::string HA_Discovery_Prefix="homeassistant";
std::string HA_Device_Name="reMarkable 2";

int main() {
    // Load Config from .env file
    std::ifstream env_file(".env");
    std::string line;
    while (std::getline(env_file, line)) {
        std::istringstream is_line(line);
        std::string key;
        if (std::getline(is_line, key, '=')) {
            std::string value;
            if (key == "MQTT_SERVER") {
                if (std::getline(is_line, value)) {
                    MQTT_SERVER = value;
                }
            }
            if (key == "MQTT_PORT") {
                if (std::getline(is_line, value)) {
                    MQTT_PORT = value;
                }
            }
            if (key == "MQTT_USER") {
                if (std::getline(is_line, value)) {
                    MQTT_USER = value;
                }
            }
            if (key == "MQTT_PASS") {
                if (std::getline(is_line, value)) {
                    MQTT_PASS = value;
                }
            }
        }
    }

    // Get Battery Status
    std::ifstream battery_file("/sys/class/power_supply/max77818_battery/capacity");
    std::string battery_status;
    std::getline(battery_file, battery_status);
    
    std::ifstream cc_file("/sys/class/power_supply/max77818_battery/charge_counter");
    std::string charge_count;
    std::getline(cc_file, charge_count);

    std::ifstream cm_file("/sys/class/power_supply/max77818_battery/charger_mode");
    std::string charger_mode;
    std::getline(cm_file, charger_mode);

    // Connect to MQTT Server
    mqtt::async_client client(MQTT_SERVER, MQTT_PORT);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);
    connOpts.set_user_name(MQTT_USER);
    connOpts.set_password(MQTT_PASS);
    mqtt::token_ptr conntok = client.connect(connOpts);
    conntok->wait();
    
    // Publish Home Assistant Discovery Message

    //topic No1
    std::string HA_Discovery_Topic_1 = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery/config";
    std::string payload1 = "{\"name\":\"" + HA_Device_Name + " Battery\",\"state_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery/state\",\"unit_of_measurement\":\"%\",\"value_template\":\"{{ value_json.battery }}\",\"device_class\":\"battery\",\"json_attributes_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery/state\",\"device\":{\"identifiers\":[\"" + HA_Device_Name + "\"],\"name\":\"" + HA_Device_Name + "\",\"manufacturer\":\"reMarkable\",\"model\":\"reMarkable 2\"}}";
    mqtt::message_ptr pubmsg1 = mqtt::make_message(HA_Discovery_Topic_1, payload1);
    pubmsg1->set_qos(1);
    client.publish(pubmsg1)->wait_for(TIMEOUT);
    //topic No2
    std::string HA_Discovery_Topic_2 = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charge_count/config";
    std::string payload2 = "{\"name\":\"" + HA_Device_Name + " Charge Count\",\"state_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charge_count/state\",\"unit_of_measurement\":\"mAh\",\"value_template\":\"{{ value_json.charge_count }}\",\"device_class\":\"battery\",\"json_attributes_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charge_count/state\",\"device\":{\"identifiers\":[\"" + HA_Device_Name + "\"],\"name\":\"" + HA_Device_Name + "\",\"manufacturer\":\"reMarkable\",\"model\":\"reMarkable 2\"}}";
    mqtt::message_ptr pubmsg2 = mqtt::make_message(HA_Discovery_Topic_2, payload2);
    pubmsg2->set_qos(1);
    client.publish(pubmsg2)->wait_for(TIMEOUT);
    //topic No3
    std::string HA_Discovery_Topic_3 = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charger_mode/config";
    std::string payload3 = "{\"name\":\"" + HA_Device_Name + " Charger Mode\",\"state_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charger_mode/state\",\"value_template\":\"{{ value_json.charger_mode }}\",\"device_class\":\"battery\",\"json_attributes_topic\":\"" + HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charger_mode/state\",\"device\":{\"identifiers\":[\"" + HA_Device_Name + "\"],\"name\":\"" + HA_Device_Name + "\",\"manufacturer\":\"reMarkable\",\"model\":\"reMarkable 2\"}}";
    mqtt::message_ptr pubmsg3 = mqtt::make_message(HA_Discovery_Topic_3, payload3);
    pubmsg3->set_qos(1);
    client.publish(pubmsg3)->wait_for(TIMEOUT);

    // Publish Battery Status
    std::string HA_Battery_Topic = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/battery/state";
    std::string HA_Battery_Payload = "{\"battery\":" + battery_status + "}";
    mqtt::message_ptr pubmsg4 = mqtt::make_message(HA_Battery_Topic, HA_Battery_Payload);
    pubmsg4->set_qos(1);
    client.publish(pubmsg4)->wait_for(TIMEOUT);
    // Publish Charge Count
    std::string HA_Charge_Count_Topic = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charge_count/state";
    std::string HA_Charge_Count_Payload = "{\"charge_count\":" + charge_count + "}";
    mqtt::message_ptr pubmsg5 = mqtt::make_message(HA_Charge_Count_Topic, HA_Charge_Count_Payload);
    pubmsg5->set_qos(1);
    client.publish(pubmsg5)->wait_for(TIMEOUT);
    // Publish Charger Mode
    std::string HA_Charger_Mode_Topic = HA_Discovery_Prefix + "/sensor/" + HA_Device_Name + "/charger_mode/state";
    std::string HA_Charger_Mode_Payload = "{\"charger_mode\":" + charger_mode + "}";
    mqtt::message_ptr pubmsg6 = mqtt::make_message(HA_Charger_Mode_Topic, HA_Charger_Mode_Payload);
    pubmsg6->set_qos(1);
    client.publish(pubmsg6)->wait_for(TIMEOUT);

    // Disconnect
    mqtt::token_ptr disconntok = client.disconnect();
    disconntok->wait();
    return 0;
}
