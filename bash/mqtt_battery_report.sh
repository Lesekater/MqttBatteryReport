####
#### DEPRECATED
#### NEEDS MOSQUITTO CLIENT, difficult to install on Remarkable
####


#!/usr/bash
#
# This script will report the battery status of the standard Linux Battery Driver 
#    (for an ReMarkable 2) to an MQTT server for use in Home Assistant.
# It is intended to be run as a service, to report the battery status every day at 00:00

# Load MQTT Configuration from .env file
MQTT_SERVER=$(grep MQTT_SERVER .env | cut -d '=' -f2)
MQTT_PORT=$(grep MQTT_PORT .env | cut -d '=' -f2)
MQTT_USER=$(grep MQTT_USER .env | cut -d '=' -f2)
MQTT_PASSWORD=$(grep MQTT_PASS .env | cut -d '=' -f2)

# Home Assistant Configuration
HA_Discovery_Prefix="homeassistant"
HA_Device_Name="reMarkable 2"

# Get the battery status
echo "Getting battery status"

CAPACITY=$(cat /sys/class/power_supply/max77818_battery/capacity)
CHARGECOUNT=$(cat /sys/class/power_supply/max77818_battery/charge_counter)
CHARGERMODE=$(cat /sys/class/power_supply/max77818_battery/charger_mode)

echo "Battery capacity: $CAPACITY"
echo "Battery charge count: $CHARGECOUNT"
echo "Battery charge mode: $CHARGERMODE"

# Publish Home Assistant MQTT Discovery message
pause=2
echo "Publishing MQTT message"

#topic_no1
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "$HA_Discovery_Prefix/sensor/$HA_Device_Name/battery_capacity/config" -m "{\"name\": \"$HA_Device_Name Battery Capacity\", \"unit_of_measurement\": \"%\", \"state_topic\": \"homeassistant/sensor/$HA_Device_Name/battery_capacity/state\", \"unique_id\": \"$HA_Device_Name-battery_capacity\", \"device\": {\"identifiers\": [\"$HA_Device_Name\"], \"name\": \"$HA_Device_Name\", \"manufacturer\": \"reMarkable\", \"model\": \"reMarkable 2\"}}"
sleep $pause
#topic_no2
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "$HA_Discovery_Prefix/binary_sensor/$HA_Device_Name/battery_charging/config" -m "{\"name\": \"$HA_Device_Name Battery Charging\", \"device_class\": \"battery_charging\", \"state_topic\": \"homeassistant/binary_sensor/$HA_Device_Name/battery_charging/state\", \"unique_id\": \"$HA_Device_Name-battery_charging\", \"device\": {\"identifiers\": [\"$HA_Device_Name\"], \"name\": \"$HA_Device_Name\", \"manufacturer\": \"reMarkable\", \"model\": \"reMarkable 2\"}}"
sleep $pause
#topic_no3
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "$HA_Discovery_Prefix/sensor/$HA_Device_Name/battery_charge_count/config" -m "{\"name\": \"$HA_Device_Name Battery Charge Count\", \"unit_of_measurement\": \"mAh\", \"state_topic\": \"homeassistant/sensor/$HA_Device_Name/battery_charge_count/state\", \"unique_id\": \"$HA_Device_Name-battery_charge_count\", \"device\": {\"identifiers\": [\"$HA_Device_Name\"], \"name\": \"$HA_Device_Name\", \"manufacturer\": \"reMarkable\", \"model\": \"reMarkable 2\"}}"
sleep $pause

# Publish the battery status
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "homeassistant/sensor/$HA_Device_Name/battery_capacity/state" -m "$CAPACITY"
sleep $pause
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "homeassistant/binary_sensor/$HA_Device_Name/battery_charging/state" -m "$CHARGERMODE"
sleep $pause
mosquitto_pub -h $MQTT_SERVER -p $MQTT_PORT -u $MQTT_USER -P $MQTT_PASSWORD -t "homeassistant/sensor/$HA_Device_Name/battery_charge_count/state" -m "$CHARGECOUNT"
sleep $pause

# Exit
echo "Done"
exit 0